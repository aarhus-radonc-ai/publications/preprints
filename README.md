# 2024
- Emma Skarsø Buhl et al.: [Data harvesting vs data farming: A study of the importance of variation vs sample size in deep learning-based auto-segmentation for breast cancer patients](https://arxiv.org/abs/2404.03369)
- Mathis Ersted Rasmussen et al.: [RadDeploy: A Framework for Integrating In-House Developed Models Seamlessly into Radiotherapy Workflows](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4819349)
- Jintao Ren et al." [Segment anything model for head and neck tumor segmentation with CT, PET and MRI multi-modality images](https://arxiv.org/abs/2402.17454)